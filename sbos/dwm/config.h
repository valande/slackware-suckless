/* See LICENSE file for copyright and license details. */

/* overrides config.def.h  */

/* appearance */
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const unsigned int gappx     = 24;       /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_black[]       = "#000000";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_paleblue[]    = "#526075";
static const char col_palegreen[]   = "#838A73";
static const char col_palered[]     = "#AE4B34";
static const char *colors[][3]      = {
	/*               fg         bg              border   */
	[SchemeNorm] = { col_gray3, col_gray1,      col_gray2 },
	[SchemeSel]  = { col_gray1, col_palegreen,  col_palegreen  },
};

/* tagging */
//static const char *tags[] = { "X", "W", "D", "M", "E" };
static const char *tags[] = { "/>", "{}", "|#", "WW", "//", ")(" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       0,            0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.65; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "]]=",      tile },    /* first entry is default */
	{ "<+>",      NULL },    /* no layout function means floating behavior */
	{ "[ ]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_palered, "-sb", col_gray1, "-sf", col_palegreen, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *lockcmd[]  = { "i3lock", "-c", col_black, "-p", "win", "-e", "-f", NULL };
static const char *browsercmd[]  = { "firefox", NULL };
static const char *codecmd[]  = { "code", NULL };
static const char *fmcmd[]  = { "pcmanfm", NULL };
static const char *mpcmd[]  = { "st", "-e", "mocp", NULL };
static const char *ampcmd[]  = { "audacious", NULL };
static const char *curacmd[]  = { "cura", NULL };
static const char *pslicercmd[]  = { "pslicer", NULL };
static const char *scmd[]  = { "sublime_text", NULL };
static const char *chromecmd[]  = { "google-chrome-stable", NULL };
static const char *topcmd[]  = { "st", "-e", "top", NULL };
static const char *scrotcmd[]  = { "scrot", NULL };

static Key keys[] = {
	/* modifier                     key        			function        argument */
	{ MODKEY,                       XK_r, 			    spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_s, 			    spawn,          {.v = scmd } },
	{ MODKEY,           			XK_t, 				spawn,          {.v = termcmd } },
	{ MODKEY,           			XK_f, 				spawn,          {.v = browsercmd } },
	{ MODKEY,           			XK_g, 				spawn,          {.v = chromecmd } },
	{ MODKEY,                       XK_c,      			spawn,          {.v = codecmd} },
	{ MODKEY,                       XK_l,      			spawn,          {.v = lockcmd} },
	{ MODKEY,                       XK_m,      			spawn,          {.v = topcmd} },
	{ MODKEY,                       XK_e,      			spawn,          {.v = fmcmd} },
	{ MODKEY,                       XK_p, 			    spawn,          {.v = mpcmd } },
	{ MODKEY,                       XK_a, 			    spawn,          {.v = ampcmd } },
	{ MODKEY,                       XK_d, 			    spawn,          {.v = curacmd } },
	{ MODKEY|ShiftMask,             XK_d, 			    spawn,          {.v = pslicercmd } },
	{ MODKEY,                       XK_v, 			    spawn,          {.v = scrotcmd } },
	{ MODKEY|ControlMask,           XK_b,      			togglebar,      {0} },
	{ MODKEY,                       XK_j,      			focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      			focusstack,     {.i = -1 } },
	{ MODKEY|ControlMask,           XK_i,      			incnmaster,     {.i = +1 } },
	{ MODKEY|ControlMask,           XK_d,      			incnmaster,     {.i = -1 } },
	{ MODKEY|ControlMask,           XK_h,      			setmfact,       {.f = -0.05} },
	{ MODKEY|ControlMask,           XK_l,      			setmfact,       {.f = +0.05} },
	{ MODKEY,           			XK_Return, 			zoom,           {0} },
	{ MODKEY,                       XK_Tab,    			view,           {0} },
	{ MODKEY|ControlMask,           XK_q,      			killclient,     {0} },
	{ MODKEY|ControlMask,           XK_t,      			setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ControlMask,           XK_f,      			setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ControlMask,           XK_m,      			setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  			setlayout,      {0} },
	{ MODKEY|ControlMask,           XK_space,  			togglefloating, {0} },
	{ MODKEY,                       XK_0,      			view,           {.ui = ~0 } },
	{ MODKEY|ControlMask,           XK_0,      			tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  			focusmon,       {.i = +1 } },
	{ MODKEY,                       XK_period, 			focusmon,       {.i = -1 } },
	{ MODKEY|ControlMask,           XK_comma,  			tagmon,         {.i = +1 } },
	{ MODKEY|ControlMask,           XK_period, 			tagmon,         {.i = -1 } },
	{ MODKEY,                       XK_minus,  			setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_plus,  			setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask|ControlMask, XK_plus, 			setgaps,        {.i = 0  } },
	{ MODKEY|ShiftMask|ControlMask, XK_minus, 			setgaps,        {.i = 0  } },
	/*
	TAGKEYS(                        XK_KP_End,          			    0)
	TAGKEYS(                        XK_KP_Down,         			    1)
	TAGKEYS(                        XK_KP_Next,         			    2)
	TAGKEYS(                        XK_KP_Left,         			    3)
	TAGKEYS(                        XK_KP_Begin,        			    4)
	TAGKEYS(                        XK_KP_Right,        			    5)
	TAGKEYS(                        XK_KP_Home,         			    6)
	TAGKEYS(                        XK_KP_Up,           			    7)
	TAGKEYS(                        XK_KP_Prior,        			    8)
	TAGKEYS(                        XK_KP_Insert,       			    9)
	*/
	TAGKEYS(                        XK_1,               			    0)
	TAGKEYS(                        XK_2,               			    1)
	TAGKEYS(                        XK_3,               			    2)
	TAGKEYS(                        XK_4,               			    3)
	TAGKEYS(                        XK_5,               			    4)
	TAGKEYS(                        XK_6,               			    5)
	/*
	TAGKEYS(                        XK_7,               			    6)
	TAGKEYS(                        XK_8,               			    7)
	TAGKEYS(                        XK_9,               			    8)
	*/
	{ MODKEY|ControlMask|ShiftMask, XK_q,      			quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

