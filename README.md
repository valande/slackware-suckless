# slackware-suckless

Main goal is to configure a minimalist, fast, stable and hopefully beautiful desktop based on a tiling window manager
for a Linux Slackware64 14.2-stable machine with multilib enabled.

Do the work taking advantage of the pretty sloc-friendly software freely published by www.suckless.org, in addition with tracking
applied patches over it.

As a plus, it creates the appropiate Slackbuild script for each of the packages, with the patches already applied onto.

---

Create and install package mini-howto (fast and dirty):

- 'slprep.sh -cv srcdir' will build a new tar.gz source tarball, together with the slackbuild resources, in a staging directory.
- Visit the www.slackbuilds.org url of the corresponding slackbuild, and check for possible compile-time and config variables.
- Inside the staging directory, 'sudo ./packagename.SlackBuild' will build the package to be installed under '/tmp' dir.
- (root) installpkg /tmp/packagename.tgz.

---

Aknowledgment:

- 'slprep.sh' inspired in 'make_slackware_live.sh' (liveslak) script by AlienBob (https://alien.slackbook.org)
- SlackBuild scripts from www.slackbuilds.org
- Applied patches inspired in those available at www.suckless.org