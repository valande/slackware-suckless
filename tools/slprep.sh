#!/bin/bash

# Copyright 2019 valande@gmail.com
#
# All rights reserved.
#
#   Permission to use, copy, modify, and distribute this software for
#   any purpose with or without fee is hereby granted, provided that
#   the above copyright notice and this permission notice appear in all
#   copies.
#
#   THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
#   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#   IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
#   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
#   OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#   SUCH DAMAGE.
# -----------------------------------------------------------------------------
#
# This script prepares a build dir for a suckless source code tree to be 
# packaged and installed following slackbuilds.org philosofy
#
# -----------------------------------------------------------------------------

# Version of the Live OS generator:
VERSION="1.0"

# Directory where slpack.sh is stored:
SLPREP_TOOLDIR=${SLPREP_TOOLDIR:-"$(cd $(dirname $0); pwd)"}

# Load the optional configuration file:
CONFFILE=${CONFFILE:-"${SLPREP_TOOLDIR}/$(basename $0 .sh).conf"}
if [ -f ${CONFFILE} ]; then
  echo "-- Loading configuration file."
  . ${CONFFILE}
fi

# Set to "YES" to send error output to the console:
DEBUG=${DEBUG:-"NO"}

# Set to "YES" in order to delete everything we have,
# and rebuild any pre-existing .sxz modules from scratch:
FORCE=${FORCE:-"NO"}

# Timestamp:
THEDATE=$(date +%Y%m%d)

#
# ---------------------------------------------------------------------------
#

# Who built the package:
SLPREP_BUILDER=${SLPREP_BUILDER:-"valande"}

# Directory where slackware-suckless project is found:
SLPREP_SLDIR=${SLPREP_SLDIR:-"/home/valande/devel/suckless"}

# Directory where sbos are found:
SLPREP_SBOS=${SLPREP_SBOS:-"${SLPREP_SLDIR}/sbos"}

# Directory where src dirs are found:
SLPREP_SRCDIR=${SLPREP_SRCDIR:-"${SLPREP_SLDIR}/src"}

# Directory where stage is prepared:
SLPREP_STAGING=${SLPREP_STAGING:-"${SLPREP_SRCDIR}/staging"}

#
# ---------------------------------------------------------------------------
#

# ---------------------------------------------------------------------------
# Define some functions.
# ---------------------------------------------------------------------------

# Clean up in case of failure:
cleanup() {
  # Clean up by unmounting our loopmounts, deleting tempfiles:
  echo "--- Cleaning up the staging area..."
  [ -d ${SLPREP_STAGING} ] && rm -rf ${SLPREP_STAGING}/* 2>${DBGOUT}
}
trap 'echo "*** $0 FAILED at line $LINENO ***"; cleanup; exit 1' ERR INT TERM

# Packages a tar.gz in stage dir from input dir
PackSrcDir () {
  if [ -d "${1}" ]; then
    local _dname="${1}"
    local _owd=$(pwd)
    _dname="${_dname##*/}"
    _tar="${_dname}.tar"
    _targz="${_tar}.gz"
    cd ${SLPREP_SRCDIR}
    echo "--- packdir: Packing '${_dname}' as ${_targz}..."
    tar cf ${_tar} ${_dname}
    gzip ${_tar}
    mv ${_targz} ${SLPREP_STAGING}/${_targz}
    cd ${_owd}
    echo "--- packdir: Packing '${_dname}' as ${_targz} done."
  else
    echo "--- packdir: Target '${_dname}' is not a valid source directory."
    cleanup
    exit 1
  fi
}

# Fetch files from sbo directory to input dir
FetchSboFiles () {
  if [ -d "${1}" ]; then
    local _dname="${1}"
    local _owd=$(pwd)
    _dname="${_dname##*/}"
    _dname="${_dname%-[0-9]*}"
    _sbodir="${SLPREP_SBOS}/${_dname}"
    if [ -d ${_sbodir} ]; then
      echo "--- FetchSboFiles: Fetching '${_dname}'..."
      cp -a ${_sbodir}/* ${SLPREP_STAGING}/
      cd ${_owd}
      echo "--- FetchSboFiles: Fetching '${_dname}' done."
    else
      echo "--- FetchSboFiles: Sbo directory '${_sbodir}' not found."
      cleanup
      exit 1
    fi
  else
    echo "--- FetchSboFiles: Target '${_dname}' is not a valid directory."
    cleanup
    exit 1
  fi
}

# ---------------------------------------------------------------------------
# Action!
# ---------------------------------------------------------------------------

mkdir -p ${SLPREP_STAGING}
unset SUCKLESSDIR || true

while getopts "hvcb:s:c" Option
do
  case $Option in
    h )
	echo "----------------------------------------------------------------"
	echo "slprep.sh $VERSION"
	echo "----------------------------------------------------------------"
        echo "Usage:"
        echo "  $0 [OPTION] ... inputdir_name "
        echo ""
        echo "The script's parameters are:"
        echo " -h                 This help."
        echo " -v                 Show debug/error output."
        echo " -b sbos_dir        Slackbuilds directory (default: ${SLPREP_SBOS^})."
        echo " -s stage_dir       Stage directory (default: ${SLPREP_STAGING^})."
        echo " -c                 Cleanup stage dir before construct."
        exit
        ;;
    v ) DEBUG="YES"
        ;;
    c ) CLEAN="YES"
        ;;
    b ) SLPREP_SBOS="${OPTARG}"
        ;;
    s ) SLPREP_STAGING="${OPTARG}"
        ;;
    * ) echo "You passed an illegal switch to the program!"
        echo "Run '$0 -h' for more help."
        exit
        ;;   # DEFAULT
  esac
done

# End of option parsing.
shift $(($OPTIND - 1))

#  $1 now references the first non option item supplied on the command line
#  if one exists.
# ---------------------------------------------------------------------------

[ "$DEBUG" = "NO" ] && DBGOUT="/dev/null" || DBGOUT="/dev/stderr"

# -----------------------------------------------------------------------------
# Some sanity checks first.
# -----------------------------------------------------------------------------

# Cleanup:
if [ "$CLEAN" = "YES" ]; then
  cleanup
  [ $# -ge 1 ] || exit
fi

# $SUCKLESSDIR mandatory param
readonly SUCKLESSDIR="${1%/}"
if [ -z ${SUCKLESSDIR} ]; then
  echo "-- slprep.sh: inputdir_name parameter missing."
  exit 1
fi

# May check arch and multilib later
#if [ "$SL_ARCH" != "x86_64" -a "$MULTILIB" = "YES" ]; then
#  echo ">> Multilib is only supported on x86_64 architecture."
#  exit 1
#fi

# -----------------------------------------------------------------------------
# Construct the stage
# -----------------------------------------------------------------------------

echo "-- Construct staging area ${SLPREP_STAGING}..."

# Create tar.gz package:
PackSrcDir ${SUCKLESSDIR}

# Fetch sbo files
FetchSboFiles ${SUCKLESSDIR}

echo "-- Construct staging area done."

# Debug stage area contents
ls -1 ${SLPREP_STAGING} > ${DBGOUT}